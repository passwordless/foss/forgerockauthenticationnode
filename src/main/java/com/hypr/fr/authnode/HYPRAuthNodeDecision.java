/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2017 ForgeRock AS.
 */

package com.hypr.fr.authnode;

import com.google.inject.assistedinject.Assisted;
import com.hypr.fr.helper.HyprApiHelper;
import com.sun.identity.idm.AMIdentity;
import com.google.common.collect.ImmutableList;
import org.forgerock.json.JsonValue;
import org.forgerock.openam.annotations.sm.Attribute;
import org.forgerock.openam.auth.node.api.*;
import org.forgerock.openam.core.CoreWrapper;
import org.forgerock.openam.sm.annotations.adapters.Password;
import org.forgerock.util.i18n.PreferredLocales;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import static org.forgerock.openam.auth.node.api.SharedStateConstants.REALM;

@Node.Metadata(outcomeProvider = HYPRAuthNodeDecision.OutcomeProvider.class,
               configClass = HYPRAuthNodeDecision.Config.class, tags = {"mfa"})

public class HYPRAuthNodeDecision extends AbstractDecisionNode {
    private static final String COMPLETED = "COMPLETED";
    private static final String CANCELED = "CANCELED";
    private static final String FAILED = "FAILED";
    private static final String WAITING = "WAITING";
    private final Logger logger = LoggerFactory.getLogger("amAuth");
    private final Config config;
    private final static String DEBUG_FILE = "HYPRAuthNodeDecision";

    public interface Config {
        @Attribute(order = 1)
        default String hyprBaseURL() {
            return "https://example.hypr.com";
        }

        @Attribute(order = 2)
        default String hyprAppId() {
            return "applicationId";
        }

        @Attribute(order = 3)
        @Password
        default char[] hyprAccessToken() {
            return "API Token".toCharArray();
        }
    }

    @Inject
    public HYPRAuthNodeDecision(@Assisted Config config) {
        this.config = config;
        logger.debug("Setting the HYPR configuration... ");
        logger.debug("HYPR Base URL: " + config.hyprBaseURL());
        logger.debug("HYPR Application ID: " + config.hyprAppId());

    }

    @Override
    public Action process(TreeContext context) {

        String username = context.sharedState.get(SharedStateConstants.USERNAME).asString();
        logger.info("HYPRAuthNodeDecision: Username: " + username);

        URL hyprUrl = null;
        try {
            hyprUrl = new URL(config.hyprBaseURL());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        JsonValue sharedState = context.sharedState.copy();
        HyprApiHelper hyprApiHelper = new HyprApiHelper(hyprUrl, config.hyprAppId(),
                                                        Arrays.toString(config.hyprAccessToken()));
        try {
            //Setup for Node to check auth status
            if (context.sharedState.get("requestId").isNotNull() && context.sharedState.get("requestId").asString().length() > 0) {
                JSONObject lastStateInArray = hyprApiHelper.getHYPRAuthStatus(username, context.sharedState.get("traceId").asString(),
                                                                              context.sharedState.get("spanId").asString(),
                                                                              context.sharedState.get("requestId").asString());
                logger.info("HYPRAuthNodeDecision: Current HYPR Auth State: {}", lastStateInArray.getString("value"));
                switch (lastStateInArray.getString("value")) {
                    case COMPLETED:
                        return goTo(COMPLETED).replaceSharedState(sharedState).build();
                    case FAILED:
                        return goTo(FAILED).replaceSharedState(sharedState).build();
                    case CANCELED:
                        return goTo(CANCELED).replaceSharedState(sharedState).build();
                    default:
                        return goTo(WAITING).replaceSharedState(sharedState).build();

                }
            } else {
                logger.info("HYPRAuthNodeDecision: We are missing the Request Id, no authentication has been passed to this node");
                return goTo(FAILED).replaceSharedState(sharedState).build();
            }
        } catch (Exception e) {
            logger.error("HYPR Error '%s' ", e);
        }
        logger.debug("If we reached this, an unexpected use case arrived");
        return goTo(FAILED).replaceSharedState(sharedState).build();
    }

    private Action.ActionBuilder goTo(String outcome) {
        return Action.goTo(outcome);
    }

    static class OutcomeProvider implements org.forgerock.openam.auth.node.api.OutcomeProvider {
        private static final String BUNDLE = "com/hypr/fr/authnode/HYPRAuthNodeDecision";
        private static final String COMPLETED = "COMPLETED";
        private static final String CANCELED = "CANCELED";
        private static final String FAILED = "FAILED";
        private static final String WAITING = "WAITING";

        @Override
        public List<Outcome> getOutcomes(PreferredLocales locales, JsonValue nodeAttributes) {
            ResourceBundle bundle = locales.getBundleInPreferredLocale(OutcomeProvider.BUNDLE, OutcomeProvider.class.getClassLoader());
            return ImmutableList.of(
                    new Outcome(COMPLETED, bundle.getString("completedOutcome")),
                    new Outcome(FAILED, bundle.getString("failedOutcome")),
                    new Outcome(CANCELED, bundle.getString("canceledOutcome")),
                    new Outcome(WAITING, bundle.getString("waitingOutcome")));
        }
    }
}