/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2017 ForgeRock AS.
 */

package com.hypr.fr.authnode;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import javax.inject.Inject;

import org.forgerock.json.JsonValue;
import org.forgerock.openam.annotations.sm.Attribute;
import org.forgerock.openam.auth.node.api.AbstractDecisionNode;
import org.forgerock.openam.auth.node.api.Action;
import org.forgerock.openam.auth.node.api.Node;
import org.forgerock.openam.auth.node.api.SharedStateConstants;
import org.forgerock.openam.auth.node.api.TreeContext;
import org.forgerock.openam.sm.annotations.adapters.Password;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.assistedinject.Assisted;
import com.hypr.fr.helper.HyprApiHelper;

@Node.Metadata(outcomeProvider = AbstractDecisionNode.OutcomeProvider.class,
               configClass = HYPRAuthNodeInitiator.Config.class, tags = {"mfa"})

public class HYPRAuthNodeInitiator extends AbstractDecisionNode {

    private final Logger logger = LoggerFactory.getLogger("amAuth");
    private final Config config;
    private final static String DEBUG_FILE = "HYPRAuthNodeInitiator";

    public interface Config {
        @Attribute(order = 1)
        default String hyprBaseURL() {
            return "https://example.hypr.com";
        }

        @Attribute(order = 2)
        default String hyprAppId() {
            return "applicationId";
        }

        @Attribute(order = 3)
        @Password
        default char[] hyprAccessToken() {
            return "API Token".toCharArray();
        }

        @Attribute(order = 4)
        default String hyprAuthAction() {
            return "defaultAuthAction";
        }
    }

    @Inject
    public HYPRAuthNodeInitiator(@Assisted Config config) {
        this.config = config;
        logger.debug("Setting the HYPR configuration... ");
        logger.debug("HYPR Base URL: " + config.hyprBaseURL());
        logger.debug("HYPR Application ID: " + config.hyprAppId());
        logger.debug("HYPR API Token: ");
    }

    @Override
    public Action process(TreeContext context) {
        String username = context.sharedState.get(SharedStateConstants.USERNAME).asString();

        URL hyprUrl = null;
        try {
            hyprUrl = new URL(config.hyprBaseURL());
        } catch (MalformedURLException e) {
            logger.error("HYPR Error '%s' ", e);
        }

        logger.info("HYPRAuthNodeInitiator: Username: " + username);

        JsonValue sharedState = context.sharedState.copy();
        JSONObject authResponseObject = null;
        String requestId = "";

        try {
            HyprApiHelper hyprApiHelper =
                    new HyprApiHelper(hyprUrl, config.hyprAppId(), Arrays.toString(config.hyprAccessToken()), config.hyprAuthAction());
            authResponseObject = hyprApiHelper
                    .sendHYPRAuth(username, context.sharedState.get("traceId").asString(), context.sharedState.get("spanId").asString());
            requestId = authResponseObject.getString("requestId");
            logger.info("HYPRAuthNodeInitiator: HYPR RequestId = " + requestId);
        } catch (Exception e) {
            logger.error("HYPR Error '%s' ", e);
        }

        if (authResponseObject != null && authResponseObject.length() > 0) {
            sharedState.put("requestId", requestId);
            return goTo(true).replaceSharedState(sharedState).build();
        } else {
            return goTo(false).replaceSharedState(sharedState).build();
        }

    }

}