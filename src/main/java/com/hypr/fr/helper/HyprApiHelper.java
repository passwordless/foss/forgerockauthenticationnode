package com.hypr.fr.helper;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;


public class HyprApiHelper {

    //TODO Not used
    private static final String COMPLETED = "COMPLETED";
    private static final String CANCELED = "CANCELED";
    private static final String FAILED = "FAILED";
    private final Logger logger = LoggerFactory.getLogger("amAuth");

    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
    private static final String NUMBER = "0123456789";

    private static final String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;
    private static final SecureRandom random = new SecureRandom();
    private static final String REQUEST_PATH = "/rp/api/oob/client/authentication/requests";
    private static final String TRX_PATH = "/rp/api/oob/client/authentication/requests/transaction";
    private static final String DEVICE_LOOKUP_PATH = "/rp/api/oob/client";
    private final String rpAppId;
    private final URL hyprBaseUrl;
    private final String accessToken;
    private String hyprAuthAction;


    public HyprApiHelper(URL hyprBaseUrl, String hyprAppId, String hyprToken ) {
        logger.debug("HYPR Base URL=" + hyprBaseUrl);
        this.hyprBaseUrl = hyprBaseUrl;
        logger.debug("HYPR rpAppId=" + hyprAppId);
        this.rpAppId = hyprAppId;
        this.accessToken = hyprToken;
    }

    public HyprApiHelper(URL hyprBaseUrl, String hyprAppId, String hyprToken, String hyprAuthAction ) {
        logger.debug("HYPR Base URL=" + hyprBaseUrl);
        this.hyprBaseUrl = hyprBaseUrl;
        logger.debug("HYPR rpAppId=" + hyprAppId);
        this.rpAppId = hyprAppId;
        this.accessToken = hyprToken;
        this.hyprAuthAction = hyprAuthAction;
    }

    public JSONObject getHYPRAuthStatus(String userName, String traceId, String spanId, String requestId) throws Exception{

        try {
            String authStatusRequest = getJson(hyprBaseUrl.toString() + REQUEST_PATH + "/" + requestId,
                    "FRPlugin-" + rpAppId + "-" + userName,
                    traceId,
                    spanId);

            if (authStatusRequest != null && authStatusRequest.length() > 0) {
                JSONObject resultJsonObject = new JSONObject(authStatusRequest);
                //If the result object does not contain the state, we want to fail.
                if (!resultJsonObject.has("state")) {
                    logger.error("Result does not contain state, returning null");
                    return null;
                }

                JSONArray stateArray = resultJsonObject.getJSONArray("state");

                return stateArray.getJSONObject(stateArray.length() - 1);

            }
        }
        catch (JSONException e){
            logger.error("Exception in HYPR ForgeRock Plugin =" + e.getMessage());
            logger.error(e.toString());
        }
        return null;

    }

    public JSONObject sendHYPRAuth(String userName, String traceId, String spanId) throws Exception{
        JSONObject json = new JSONObject();
        try{
            json.put("machineId", "ForgeRock-" + rpAppId + "-" + userName);
            json.put("namedUser", userName);
            json.put("machine", "ForgeRock HYPR True Passwordless Security");
            json.put("sessionNonce", doSha256(generateRandomPIN() + ""));
            json.put("deviceNonce", doSha256(generateRandomPIN() + ""));
            json.put("serviceNonce", doSha256(generateRandomPIN() + ""));
            json.put("serviceHmac", doSha256(generateRandomPIN() + ""));
            json.put("appId", rpAppId);

            if ( hyprAuthAction == null) { hyprAuthAction="defaultAuthAction"; }
            if(!hyprAuthAction.isEmpty()) {
                json.put("actionId", hyprAuthAction);
            }else{
                json.put("actionId", "defaultAuthAction");
            }

            String payload = json.toString();
            logger.debug("Sending Payload for PUSH: " + payload);

            String jsonResult = sendJson(hyprBaseUrl.toString() + REQUEST_PATH,
                    payload ,
                    traceId,
                    spanId);

            if(jsonResult != null) {

                JSONObject requestInitResponse = new JSONObject(jsonResult);
                return requestInitResponse.getJSONObject("response");
            }
                return null;
        }
        catch (JSONException e){
            logger.error("Exception in HYPR ForgeRock Plugin =" + e.getMessage());
            logger.error(e.toString());
        }
        return null;

    }

    public JSONArray checkForRegisteredDevices(String userName, String traceId, String spanId) throws Exception {
        logger.info("Enter check for registered devices");
        try {
            logger.debug("About to make REST call to get user devices.");

            String jsonResult = getJson(hyprBaseUrl.toString() + DEVICE_LOOKUP_PATH + "/" + userName + "/devices",
                    "FRPlugin-" + rpAppId + "-" + userName,
                    traceId,
                    spanId);

            return new JSONArray(jsonResult);
        } catch (Exception e) {
            logger.error("checkForRegisteredDevices():  Error making check transaction details API call", e);
            throw new Exception("checkForRegisteredDevices():  Error making check transaction details API call", e);
        }
    }


    public String getJson(String targetUrl, String machineId, String traceId, String spanId) throws IOException {
        logger.debug("Making REST call to: " + targetUrl);
        logger.info("Making REST call to: " + targetUrl);

        URL myurl = new URL(targetUrl);
        HttpURLConnection con = (HttpURLConnection)myurl.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);

        //Add Authorization Header if access token is not empty
        if ((accessToken != null) && (!accessToken.isEmpty())){
            con.setRequestProperty("Authorization", "Bearer " + accessToken);
        }
        con.setRequestProperty("X-B3-TraceId", traceId);
        con.setRequestProperty("X-B3-SpanId", spanId);
        con.setRequestProperty("Content-Type", "application/json;");
        con.setRequestProperty("HYPR-machineId", machineId);
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("Method", "GET");
        logger.info("Connection Details: " + con.getURL().toString());


        StringBuilder sb = new StringBuilder();
        int HttpResult =con.getResponseCode();
        if(HttpResult ==HttpURLConnection.HTTP_OK){
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            br.close();
            logger.debug("Got REST GET result=" + sb);

            return sb.toString();

        }else{
            logger.error(String.valueOf(con.getResponseCode()));
            logger.error(con.getResponseMessage());
            System.out.println(con.getResponseMessage());

            return null;
        }
    }

    public String sendJson(String targetUrl, String json, String traceId, String spanId) throws IOException {

        //TODO Duplicate code from above
        URL myurl = new URL(targetUrl);
        HttpURLConnection con = (HttpURLConnection)myurl.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);

        //Add Authorization Header if access token is not empty
        if ((accessToken != null) && (!accessToken.isEmpty())){
            con.setRequestProperty("Authorization", "Bearer " + accessToken);
        }

        con.setRequestProperty("X-B3-TraceId", traceId);
        con.setRequestProperty("X-B3-SpanId", spanId);

        con.setRequestProperty("Content-Type", "application/json;");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("Method", "POST");

        OutputStream os = con.getOutputStream();
        os.write(json.getBytes(StandardCharsets.UTF_8));
        os.close();


        StringBuilder sb = new StringBuilder();
        int HttpResult =con.getResponseCode();
        if(HttpResult ==HttpURLConnection.HTTP_OK){
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            br.close();
            logger.debug("Got REST POST result=" + sb);

            return sb.toString();

        }else{
            logger.error(String.valueOf(con.getResponseCode()));
            logger.error(con.getResponseMessage());
            return null;
        }

    }


    //TODO Never used
    public static String generateRandomString(int length) {
        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {

            // 0-62 (exclusive), random returns 0-61
            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);

            sb.append(rndChar);

        }

        return sb.toString();

    }

    private boolean isEmpty(String str) {
        return str == null || str.isEmpty();

    }



    public static String getHexDigits(){
        Random random = new Random();
        byte[] randomBytes = new byte[8];
        random.nextBytes(randomBytes);
        return encodeHexString(randomBytes);
    }


    public static String encodeHexString(byte[] byteArray) {
        StringBuilder hexStringBuffer = new StringBuilder();
        for (byte b : byteArray) {
            hexStringBuffer.append(byteToHex(b));
        }
        return hexStringBuffer.toString();
    }
    public static  String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }

    public static int generateRandomPIN() {
        return 100000 + (new Random()).nextInt(900000);
    }

    public static String doSha256(String stringToHash) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(stringToHash.getBytes());
        byte[] bytes = md.digest();
        StringBuilder hexString = new StringBuilder();

        for (byte aByte : bytes) {
            String hex = Integer.toHexString(255 & aByte);
            if (hex.length() == 1) {
                hexString.append('0');
            }

            hexString.append(hex);
        }

        return hexString.toString();
    }

}
