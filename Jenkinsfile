pipeline {
    agent {label 'ls1'}

    triggers {
        // POll Git for changes every 5 minutes
        pollSCM('H/5 * * * *')
    }

    environment {
        ARTIFACT_ID = "HyprAuthNode"
        ARTIFACT_VERSION = "${(readFile('pom.xml') =~ '<version>(.+)</version>')[0][1]}"
        ARTIFACT_PACKAGING = "jar"
        ARTIFACT = "${ARTIFACT_ID}-${ARTIFACT_VERSION}.${ARTIFACT_PACKAGING}"

        NEXUS_URL = "https://nexus.hypr.com"
        NEXUS_REPO = "hypr-maven-dev"

        // GIT_COMMIT env variable is exposed by the Git plugin
        GIT_COMMIT_SHORT = "${env.GIT_COMMIT}".trim().substring(0, 8)

        // BRANCH_NAME env variable is exposed by the Jenkins Pipeline
        GIT_BRANCH_SHORT = "${env.BRANCH_NAME.contains("/") ? env.BRANCH_NAME.split("/")[1] : env.BRANCH_NAME}"
    }

    stages {
        stage('Build') {
            steps {
                echo "********** Building **********"
                echo "GIT_COMMIT_SHORT: ${GIT_COMMIT_SHORT} | GIT_BRANCH_SHORT: ${GIT_BRANCH_SHORT} | NEXUS_URL: ${NEXUS_URL}"

                configFileProvider(
                        [configFile(fileId: 'HYPR_MAVEN_JENKINS_CONFIG', variable: 'MAVEN_SETTINGS')]) {

                    sh '''mvn -U -s $MAVEN_SETTINGS clean package -DGIT_COMMIT_SHORT=${GIT_COMMIT_SHORT} -DGIT_BRANCH_SHORT=${GIT_BRANCH_SHORT}'''
                }
            }
            post {
                always {
                    echo "Pick up maven Surefire plugin reports"
                    //junit 'target/surefire-reports/**/*.xml'
                }
            }
        }

        stage('Nexus') {
            steps {
                // Maven config file is setup using the Config files plugin
                // MAVEN_JENKINS_CONFIG is the ID of the config entry maven settings.xml
                configFileProvider(
                        [configFile(fileId: 'HYPR_MAVEN_JENKINS_CONFIG', variable: 'MAVEN_SETTINGS')]) {

                    sh '''
                        mvn -s $MAVEN_SETTINGS deploy:deploy-file -B \
                        -DgroupId=com.hypr.fr \
                        -DartifactId=${ARTIFACT_ID}-${GIT_BRANCH_SHORT} \
                        -Dversion=${ARTIFACT_VERSION} \
                        -DgeneratePom=true \
                        -Dpackaging=${ARTIFACT_PACKAGING} \
                        -DrepositoryId=${NEXUS_REPO} \
                        -Durl=${NEXUS_URL}/repository/${NEXUS_REPO} \
                        -Dfile=target/${ARTIFACT}
                        '''
                }
            }
        }
    }
}
