

# HYPRAuthNode

HYPR and [ForgeRock](https://www.forgerock.com/) have partnered to deliver a true passwordless authentication experience to the enterprise. The HYPR solution ensures that your users’ credentials always remain safe on personal devices. Eliminating centralized passwords enables HYPR to remove the target and provide a secure password-less experience for your customers and employees.

By decentralizing user authentication, HYPR minimizes the risk of a breach, eliminates credential reuse, and enables enterprises to Trust Anyone.

## Pre-Requisites
* Have a deployed ForgeRock OpenAM 6.0+ instance deployed
* Have a deployed HYPR Server with access to the HYPR Control Center
* Have an application in the Control Center created for ForgeRock

Contact HYPR or ForgeRock for any assistance in the deployment of HYPR or ForgeRock OpenAM.

# Installation

To install the HYPR Authentication Node to ForgeRock you will want to follow these steps:
1. Stop the instance of Tomcat hosting ForgeRock
2. Copy the HYPR node .jar file to `../tomcat/webapps/openam/WEB-INF/lib/`
3. Start the instance of Tomcat hosting ForgeRock

## Setup
With the HYPR Authentication Node deployed within the OpenAM installation you will be able to create an authentication tree which utilizes HYPR for authentication.

A Sample user journey is depicted below
![img.png](readme-images/img.png)

Required nodes for building HYPR Passwordless User Journey
1. HYPR Auth Check For Device
2. HYPR Auth Initiator
3. Polling Wait Node
4. HYPR Auth Decision
5. Retry Limit Decision

### HYPR Auth Check For Device Configuration
![img.png](readme-images/HYPRAuthCheckForDevice.png)

Requires:

**HYPR Base URL**: https://hypr.host.com

**HYPR App Id**: RP AppId which will be used in HYPR to register and authenticate users

**HYPR API Token**: An Access Token generated from within the HYPR Control Center for the specific RP Application

### HYPR Auth Initiator
![img.png](readme-images/HYPRAuthInitiator.png)

Requires:

**HYPR Base URL**: https://hypr.host.com

**HYPR App Id**: RP AppId which will be used in HYPR to register and authenticate users

**HYPR API Token**: An Access Token generated from within the HYPR Control Center for the specific RP Application

Default or Optional:

**HYPR Authentication Policy**: The default is set for supporting the HYPR Mobile application. If the HYPR implementation is supporting a consumer or customer implementation then this will need to match the policy set in the HYPR Control Centers RP Application

### Polling Wait Node
![img.png](readme-images/PollingWaitNode.png)

Requires:

**Seconds To Wait**: This is the increment in time till making another Authentication status check against HYPR
**Waiting Message**: Custom Message to present to users to let them know a push notification has been sent to their HYPR Mobile Application

Waiting Message Example
![img.png](readme-images/WaitingMessageExample.png)

### HYPR Auth Decision
![img.png](readme-images/HYPRAuthDecision.png)

Requires:

**HYPR Base URL**: https://hypr.host.com

**HYPR App Id**: RP AppId which will be used in HYPR to register and authenticate users

**HYPR API Token**: An Access Token generated from within the HYPR Control Center for the specific RP Application

### Retry Limit Decision
![img.png](readme-images/RetryLimitDecision.png)

Requires:

**Retry Limit**: This should be set to be no more that the Polling Wait Node (Seconds to Wait * Retry Limit) = 60 seconds in total.